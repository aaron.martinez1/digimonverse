# Digimonverse

Are you up for the challenge? Begin your Digimonverse now!

![screenshot-game](./screenshot-game.png)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.2.7.

1. Ren `git clone [urlRepo]` for clone repo.
2. Run `npm i` for install all dependencies.
3. `cd /digimonverse` 
3. Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable()

export class ApiHelper {

  urlApi = environment.apiUrl;
  config: any;

  constructor(
    public http: HttpClient
  ) {
  }

  get(endpoint: string, params?: any) {
    if (params) {
        return this.http.get<any>(this.urlApi + '/' + endpoint, {params: this.toHttpParams(params)});
      } else {
        return this.http.get<any>(this.urlApi + '/' + endpoint);
      }
    return this.http.get(this.urlApi + '/' + endpoint);
  }

  toHttpParams(obj: any): HttpParams {
    return Object.getOwnPropertyNames(obj)
      .reduce((p, key) => p.set(key, obj[key]), new HttpParams());
  }

}
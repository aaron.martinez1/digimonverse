import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DigimonComponent } from '../pages/digimon/digimon.component';

@Injectable({
  providedIn: 'root'
})
export class RouterHelper {


  constructor(
    public dialog: MatDialog
  ) {
  }

  goDigimonComponent(id: string) {
    this.dialog.open(DigimonComponent, {
      panelClass: 'fullscreen-dialog',
      data: {id: id}
    });
  }
  
}

import { Component, Input } from '@angular/core';
import { RouterHelper } from 'src/app/helpers/router.helper';
import { DigimonComponent } from 'src/app/pages/digimon/digimon.component';

@Component({
  selector: 'app-item-digimon-lista',
  templateUrl: './item-digimon-lista.component.html',
  styleUrls: ['./item-digimon-lista.component.scss']
})
export class ItemDigimonListaComponent {

  @Input() digimons:any;
  
  constructor(
    public routerHelper: RouterHelper
  ) { }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemDigimonListaComponent } from './item-digimon-lista.component';

describe('ItemDigimonListaComponent', () => {
  let component: ItemDigimonListaComponent;
  let fixture: ComponentFixture<ItemDigimonListaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ItemDigimonListaComponent]
    });
    fixture = TestBed.createComponent(ItemDigimonListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

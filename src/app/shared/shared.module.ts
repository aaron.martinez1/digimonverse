import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemDigimonListaComponent } from './item-digimon-lista/item-digimon-lista.component';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';
import { NotFoundComponent } from './not-found/not-found.component'; 

@NgModule({
  declarations: [
    ItemDigimonListaComponent,
    NotFoundComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatDialogModule 
  ],
  exports: [
    ItemDigimonListaComponent,
    NotFoundComponent
  ]
})
export class SharedModule {}

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { RouterHelper } from 'src/app/helpers/router.helper';
import { DigimonService } from 'src/app/services/digimon.service';

@Component({
  selector: 'app-digimon',
  templateUrl: './digimon.component.html',
  styleUrls: ['./digimon.component.scss']
})
export class DigimonComponent {

  digimon:any;

  constructor( 
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<DigimonComponent>,
    private digimonService: DigimonService,
    private routerHelper: RouterHelper
    ) { 
      this.getDigimon();
    }

    getDigimon() {
      this.digimonService.getDigimonById(this.data.id).subscribe((response: any) => {
        this.digimon = response;
      });
    }

    getEnglishDescription() {
      for (const description of this.digimon.descriptions) {
        if (description.language === 'en_us') {
          return description.description;
        }
      }
      return 'Not description found'; 
    }

    getLevels() {
      if (this.digimon.levels) {
        const levelNames = this.digimon.levels.map((level:any) => level.level);
        if (levelNames.length > 0) {
          return levelNames.join(', ');
        }
      }
      return 'No levels found';
    }
    
    getAttributes() {
      if (this.digimon.attributes) {
        const attributeNames = this.digimon.attributes.map((attribute:any) => attribute.attribute);
        if (attributeNames.length > 0) {
          return attributeNames.join(', ');
        }
      }
      return 'No attributes found';
    }

    getTypes() {
      for (const type of this.digimon.types) {
          return type.type;
      }
      return 'Not type found'; 
    }

    getReleaseData() {
      if(this.digimon.releaseDate){
        return this.digimon.releaseDate;
      }
      return 'Not release data found'; 
    }

    showDetail(idDigimon:any) {
      this.routerHelper.goDigimonComponent(idDigimon);
    }
    

    close() { 
      this.dialogRef.close(); 
    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { DigimonComponent } from './digimon/digimon.component';
import { HelpComponent } from './help/help.component';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';


@NgModule({
  declarations: [
    HomeComponent,
    SearchComponent,
    DigimonComponent,
    HelpComponent,
    PageNotFoundComponentComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
    RouterModule
  ],
})
export class PagesModule { }

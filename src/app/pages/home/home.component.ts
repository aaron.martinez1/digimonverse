import { Component, OnInit } from '@angular/core';
import { interval, switchMap } from 'rxjs';
import { RouterHelper } from 'src/app/helpers/router.helper';
import { DigimonService } from 'src/app/services/digimon.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit{

  digimon:any;
  showPopover: boolean = false;

  constructor(
    public routerHelper: RouterHelper,
    private digimonService: DigimonService,
  ) {
    this.getRandomDigimon();
  }


  ngOnInit() {
    interval(15000).pipe(
      switchMap(() => this.digimonService.getRandomDigimon())
    ).subscribe((data: any) => {
      this.digimon = data;
    });
  }

  getRandomDigimon() {
    this.digimonService.getRandomDigimon().subscribe((response: any) => {
        this.digimon = response;
    });
  }

}

import { Component } from '@angular/core';
import { forkJoin } from 'rxjs';
import { DigimonService } from 'src/app/services/digimon.service';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent {

  attributeObject:any;
  fields:any;
  levels:any;
  types:any;

  categories: any[] = [];
  
  constructor(
    private digimonService: DigimonService,
  ) {
    this.getCategories();
   }


   getCategories() {
    forkJoin(
      this.digimonService.getAttributes(),
      this.digimonService.getFields(),
      this.digimonService.getLevels(),
      this.digimonService.getTypes(),
      this.digimonService.getSkills()
    ).subscribe(([attributes, fields, levels, types, skills]) => {
      this.categories = [attributes.content, fields.content, levels.content, types.content, skills.content];
    });
  }

}

import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Observable, catchError, map, of, switchMap, tap } from 'rxjs';
import { DigimonService } from 'src/app/services/digimon.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  
  showScrollTopButton = false;

  attributes: any[] = [];
  levels: any[] = [];
  pageSizeOptions: number[] = [10, 20, 50, 100];

  searchParamsForm = this.formBuilder.group({
    name: '',
    attribute: '',
    level: '',
    page: 0,
    pageSize: 10,
  });

  actualPage: number = 0;
  showViewMore: boolean = true;

  notResults: boolean = false;
  isLoading:boolean = true;

  digimons: any = [];

  digimons$: Observable<any> = this.searchParamsForm.valueChanges.pipe(
    switchMap((searchParamsForm: any) => {
      const queryParams:any = {};

      // Por limitación de API solo se envia si se ha seleccionado un attribute
      if (searchParamsForm.attribute) { 
        queryParams['attribute'] = searchParamsForm.attribute;
      }
      // Por limitación de API Solo se envia si se ha seleccionado un level
      if (searchParamsForm.level) {  
        queryParams['level'] = searchParamsForm.level;
      }
      queryParams['name'] = searchParamsForm.name;
      queryParams['page'] = searchParamsForm.page;
      queryParams['pageSize'] = searchParamsForm.pageSize;

      return this.digimonService.getDigimons(queryParams);
    }),
    tap((result: any) => { 
      this.actualPage = result.pageable.currentPage
      this.showViewMore = result.pageable.currentPage < result.pageable.totalPages - 1;
    }),
    map((res: any) => res.content),
    tap((result) => {
      this.isLoading = false;
      this.notResults = false;
      if(!result){
        this.notResults = true;
        this.digimons = [];
        return;
      }
      if (this.searchParamsForm.controls.page.value == 0) {
        this.digimons = result;
      } else {
        for (const item of result) {
          this.digimons.push(item);
        }
      }
    }),
    catchError((errorResponse) => {
      this.isLoading = false;
      this.notResults = false;
      console.error(errorResponse);
      return of(null);
    })
  );


  constructor(
    private formBuilder: FormBuilder,
    private digimonService: DigimonService
  ) {
    this.digimons$.subscribe();
  }

  ngOnInit() {
    this.getAttributes();
    this.getLevels(); 
    this.searchParamsForm.controls.name.setValue('');
  }
  
  loadMore() {
    if (!this.isLoading) {
      this.isLoading = true;
      this.searchParamsForm.controls.page.setValue(
        this.actualPage + 1
      );
    }
  }

  getAttributes() {
    this.digimonService.getAttributes().subscribe((response: any) => {
      this.attributes = response.content.fields;
    });
  }

  getLevels() {
    this.digimonService.getLevels().subscribe((response: any) => {
      this.levels = response.content.fields;
    });
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const yOffset = window.scrollY; 
    this.showScrollTopButton = yOffset > 2000; 
  }

  scrollTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }
}
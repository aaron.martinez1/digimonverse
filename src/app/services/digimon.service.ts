import { Injectable } from '@angular/core';
import { ApiHelper } from '../helpers/api.helper';
import { tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DigimonService {

  totalDigimons: number = 1422;

  constructor(
    private api: ApiHelper
  ) { }


  getDigimonById(id: number){
    return this.api.get('digimon/' + id).pipe(
      tap( digimon => {
        console.log('---DIGIMON---', digimon); 
      })
    );
  }

  getRandomDigimon(){
    const randomIdDigimon = Math.floor(Math.random() * this.totalDigimons) + 1;
    return this.api.get('digimon/' + randomIdDigimon).pipe(
      tap( digimon => {
        console.log('---DIGIMON---', digimon); 
      })
    );
  }

  getDigimons(params: any){
    return this.api.get('digimon', params ).pipe(
      tap( digimons => {
        console.log('---DIGIMONS LIST---', digimons); 
      })
    );
  }


  getAttributes(){
    return this.api.get('attribute').pipe(
      tap( attributes => {
        console.log('---ATTRIBUTE LIST---', attributes); 
      })
    );
  }

  getLevels(){
    return this.api.get('level').pipe(
      tap( levels => {
        console.log('---LEVEL LIST---', levels); 
      })
    );
  }

  getFields(){
    return this.api.get('field').pipe(
      tap( fields => {
        console.log('---FIELD LIST---', fields); 
      })
    );
  }

  getTypes(){
    return this.api.get('type').pipe(
      tap( type => {
        console.log('---TYPE LIST---', type); 
      })
    );
  }

  getSkills(){
    return this.api.get('skill').pipe(
      tap( skills => {
        console.log('---SKILL LIST---', skills); 
      })
    );
  }


}

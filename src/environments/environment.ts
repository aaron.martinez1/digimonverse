export const environment = {
  production: false,
  apiUrl: 'https://digimon-api.com/api/v1',
};